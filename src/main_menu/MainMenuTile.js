import React from 'react';
import { Link } from 'react-router-dom';

import styled from "styled-components";


const MenuTileTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.2em;
  display: inline;
  padding-top: 0.5em;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  
  @media (max-width: 600px) {
    padding-top: 0.25em;
    font-size: 1.0em;
  }
`;

const MainMenuTileDiv = styled.div`
  border: solid thin;
  width: 20em;
  height: 20em;
  margin: 0em;
  display: flex;
  flex-direction: column;
  
  @media (max-width: 600px) {
    width: 11em;
    height: 11em;
  }
`;

const MainMenuTileLink = styled(Link)`
  margin: 1em;
  font-style: normal;
  color: black;
  text-decoration: none;
  
  @media (max-width: 600px) {
    margin: 0.3em
  }
  
  &:hover {
    background-color: lightgray;
  }
`;

const MainMenuIcon = styled.i`
  font-size: 10em;
  margin: auto;
  
  @media (max-width: 600px) {
    font-size: 5em;
  }
`;

export const MainMenuTile = (props) => {
  const menuItem = props.menuItem;
  return (
    <MainMenuTileLink to={ menuItem.url }>
      <MainMenuTileDiv>
        <MenuTileTitle>{ menuItem.title }</MenuTileTitle>
        <MainMenuIcon className={ menuItem.iconClass } />
      </MainMenuTileDiv>
    </MainMenuTileLink>
  );
};
