import React from 'react';
import styled from "styled-components";

import { MainMenuTile } from "./MainMenuTile";


const menuItems = [
  {
    title: 'Puppets',
    url: '/puppets',
    iconClass: 'fas fa-users'
  },
  {
    title: 'Choreographies',
    url: '/choreographies',
    iconClass: 'fas fa-project-diagram'
  },
  {
    title: 'Choreography Builder',
    url: '/choreography_builder',
    iconClass: 'fab fa-creative-commons-remix'
  },
  {
    title: 'Puppets Builder',
    url: '/puppet_builder',
    iconClass: 'fas fa-user-plus'
  },
  {
    title: 'Step Motor Controller',
    url: '/step_motor_controllers',
    iconClass: 'fas fa-car-side'
  }
];

const MainMenuDiv = styled.div`
  display: flex;
  flex-direction: column;
`;

const MainMenuTilesDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

const MainMenuTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
  text-align: center;
`;

const MainMenuTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
`;

export class MainMenu extends React.Component {
  render() {
    const menuTiles = this.createMenuTiles();
    return (
      <MainMenuDiv>
        <MainMenuTitleDiv>
          <MainMenuTitle>Welcome</MainMenuTitle>
        </MainMenuTitleDiv>
        <MainMenuTilesDiv>
          { menuTiles }
        </MainMenuTilesDiv>
      </MainMenuDiv>
    );
  };

  createMenuTiles() {
    return menuItems.map((menuItem, index) => (
      <MainMenuTile menuItem={ menuItem } key={ index } />
    ));
  }
}
