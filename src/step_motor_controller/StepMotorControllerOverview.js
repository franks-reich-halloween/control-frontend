import React from 'react';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from "styled-components";
import { curry } from 'ramda';

import { StepMotorControllerTile } from "./StepMotorControllerTile";


const StepMotorControllerOverviewDiv = styled.div`
  display: flex;
  flex-direction: column;
`;

const StepMotorControllerTilesDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

const StepMotorControllerTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
  text-align: center;
`;

const StepMotorControllerOverviewTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
`;

export class StepMotorControllerOverview extends React.Component {
  constructor() {
    super();
    this.state = {
      stepMotorControllers: []
    }

    this.onStartStepMotorController = this.onStartStepMotorController.bind(this);
    this.onStopStepMotorController = this.onStopStepMotorController.bind(this);
    this.fetchStepMotorControllers = this.fetchStepMotorControllers.bind(this);
  }

  componentDidMount() {
    this.fetchStepMotorControllers();
  }

  fetchStepMotorControllers() {
    const url = '/halloween/step_motor_controller';
    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ stepMotorControllers: data }));
  }

  render() {
    const stepMotorControllerTiles = this.createStepMotorControllerTiles();
    return (
      <StepMotorControllerOverviewDiv>
        <StepMotorControllerTitleDiv>
          <StepMotorControllerOverviewTitle>
            Step Motor Controller
          </StepMotorControllerOverviewTitle>
        </StepMotorControllerTitleDiv>
        <StepMotorControllerTilesDiv>
          { stepMotorControllerTiles }
        </StepMotorControllerTilesDiv>
      </StepMotorControllerOverviewDiv>
    );
  }

  createStepMotorControllerTiles() {
    const stepMotorControllers = this.state.stepMotorControllers;
    return stepMotorControllers.map((stepMotorController, index) => {
      const onStartMotorController = curry(this.onStartStepMotorController)(
        stepMotorController.identifier);
      const onStopMotorController = curry(this.onStopStepMotorController)(
        stepMotorController.identifier);
      return (
        <StepMotorControllerTile
          key={ index }
          stepMotorController={ stepMotorController }
          onStopMotorController={ onStopMotorController }
          onStartMotorController={ onStartMotorController } />
      );
    });
  }

  onStartStepMotorController(identifier, event) {
    const url = '/halloween/step_motor_controller/' + identifier + '/start_repeat';
    fetch(url, { method: 'POST' })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          this.fetchStepMotorControllers();
        }
      });
  }

  onStopStepMotorController(identifier, event) {
    const url = '/halloween/step_motor_controller/' + identifier + '/stop_repeat';
    fetch(url, { method: 'POST' })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          this.fetchStepMotorControllers();
        }
      });
  }
}
