import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from 'react-router-dom';


const StepMotorControllerTileToolbarDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 2.5em;
  flex-grow: 1;
  justify-content: flex-end;
  
  @media (max-width: 600px) {
    padding: 0.4em 2.25em;
  }
`;

const StepMotorControllerTileToolbarIcon = styled.i`
  font-size: 1.2em;
`;

const StyledButton = styled.button`
  height: 100%;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <button onClick={ onClick }>
      <StepMotorControllerTileToolbarIcon className={ iconClass } />
      { text }
    </button>
  );
};

const LinkButton = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  return (
    <StyledButton>
      <StepMotorControllerTileToolbarIcon className={ iconClass } />
      { text }
    </StyledButton>
  );
};

const Spacer = styled.div`
  flex-grow: 1;
`;

const SmallSpacer = styled.div`
  width: 0.5em;
`;

export const StepMotorControllerTileToolbar = (props) => {
  const running = props.stepMotorController.running;
  const onStartMotorController = props.onStartMotorController;
  const onStopMotorController = props.onStopMotorController;
  const playStopButton = running ?
    <Button iconClass='fas fa-stop' onClick={ onStopMotorController } />:
    <Button iconClass='fas fa-play' onClick={ onStartMotorController } />;
  const editUrl = '/step_motor_controller_builder/' + props.stepMotorController.identifier;
  return (
    <StepMotorControllerTileToolbarDiv>
      <Link to={ editUrl }>
        <LinkButton
          iconClass="fas fa-edit" />
      </Link>
      <Spacer />
      { playStopButton }
    </StepMotorControllerTileToolbarDiv>
  )
};

