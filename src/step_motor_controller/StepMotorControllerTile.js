import React from 'react';
import { Link } from 'react-router-dom';

import styled from "styled-components";

import { StepMotorControllerTileToolbar } from "./StepMotorControllerTileToolbar";


const StepMotorControllerName = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.2em;
  display: inline;
  padding-top: 0.5em;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex-grow: 1;
  
  @media (max-width: 600px) {
    padding-top: 0.25em;
    font-size: 1.0em;
  }
`;

const StepMotorControllerTileDiv = styled.div`
  border: solid thin;
  width: 20em;
  height: 20em;
  margin: 1em;
  display: flex;
  flex-direction: column;
  
  @media (max-width: 600px) {
    width: 11em;
    height: 11em;
    margin: 0.3em;
  }
`;

const StepMotorControllerChildrenDiv = styled.div`
  border: solid thin;
  width: 15em;
  height: 15em;
  align-self: center;
  
  @media (max-width: 600px) {
    width: 6.5em;
    height: 6.5em;
  }
`;

export const StepMotorControllerTile = (props) => {
  const stepMotorController = props.stepMotorController;
  const name = stepMotorController.name;
  const onStartMotorController = props.onStartMotorController;
  const onStopMotorController = props.onStopMotorController;
  return (
    <StepMotorControllerTileDiv>
      <StepMotorControllerName>{ name }</StepMotorControllerName>
      <StepMotorControllerChildrenDiv />
      <StepMotorControllerTileToolbar
        onStartMotorController={ onStartMotorController }
        onStopMotorController={ onStopMotorController }
        stepMotorController={ stepMotorController } />
    </StepMotorControllerTileDiv>
  );
};
