import React from 'react';
import { Link } from 'react-router-dom';

import styled from "styled-components";

import { PuppetTileToolBar } from "./PuppetTileToolBar";


const PuppetName = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.2em;
  display: inline;
  padding-top: 0.5em;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex-grow: 1;
  
  @media (max-width: 600px) {
    padding-top: 0.25em;
    font-size: 1.0em;
  }
`;

const PuppetTileDiv = styled.div`
  border: solid thin;
  width: 20em;
  height: 20em;
  margin: 1em;
  display: flex;
  flex-direction: column;
  
  @media (max-width: 600px) {
    width: 11em;
    height: 11em;
    margin: 0.3em;
  }
`;

const PuppetImage = styled.img`
  width: 15em;
  height: 15em;
  align-self: center;
  
  @media (max-width: 600px) {
    width: 6.5em;
    height: 6.5em;
  }
`;

export const PuppetTile = (props) => {
  const onDeletePuppet = props.onDeletePuppet;
  const puppet = props.puppet;
  const identifier = puppet.identifier;
  const name = puppet.name;
  const imageSource = 'halloween/puppet/image/' + identifier;
  return (
    <PuppetTileDiv>
      <PuppetName>{ name }</PuppetName>
      <PuppetImage src={ imageSource } />
      <PuppetTileToolBar
        puppet={ puppet }
        onDeletePuppet={ onDeletePuppet } />
    </PuppetTileDiv>
  );
};

const AddTileInnerDiv = styled.div`
  width: 20em;
  height: 20em;
  margin: 0em;
  display: flex;
  flex-direction: column;
  
  @media (max-width: 600px) {
    width: 11em;
    height: 11em;
  }
`;

const AddIcon = styled.i`
  font-size: 15em;
  margin: auto;
  
  @media (max-width: 600px) {
    font-size: 6.5em;
  }
`;

const PuppetBottomSpaceDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 0.4em 0.4em 0.4em;
  flex-grow: 1;
  justify-content: flex-end;
  
  @media (max-width: 600px) {
    padding: 0.4em 0.4em 0.4em 0.4em;
  }
`;

const TileLink = styled(Link)`
  margin: 0;
  font-style: normal;
  color: black;
  text-decoration: none;
  
  &:hover {
    background-color: lightgray;
  }
`;

export const PuppetAddTile = () => {
  return (
    <PuppetTileDiv>
      <TileLink to="/puppet_builder">
        <AddTileInnerDiv>
          <PuppetName>New Puppet</PuppetName>
          <AddIcon className="fas fa-plus-circle" />
          <PuppetBottomSpaceDiv />
        </AddTileInnerDiv>
      </TileLink>
    </PuppetTileDiv>
  );
};
