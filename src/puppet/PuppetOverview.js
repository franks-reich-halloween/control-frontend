import React from 'react';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from "styled-components";
import { curry } from 'ramda';

import { PuppetTile } from "./PuppetTile";
import { PuppetAddTile } from "./PuppetTile";
import { Popup } from "../popup/Popup";


const PuppetOverviewDiv = styled.div`
  display: flex;
  flex-direction: column;
`;

const PuppetOverviewTilesDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

const PuppetOverviewTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
  text-align: center;
`;

const PuppetOverviewTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
`;

export class PuppetOverview extends React.Component {
  constructor() {
    super();
    this.state = {
      puppets: [],
      deleteConfirmation: {
        visible: false,
        data: -1
      }
    };

    this.onDeletePuppet = this.onDeletePuppet.bind(this);
    this.onConfirmPopup = this.onConfirmPopup.bind(this);
    this.onCancelPopup = this.onCancelPopup.bind(this);
  }

  componentDidMount() {
    const url = '/halloween/puppet';
    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ puppets: data }));
  }

  render() {
    const puppetTiles = this.createPuppetTiles();
    const popup = this.state.deleteConfirmation.visible
      ? <Popup
        onCancel={ this.onCancelPopup }
        onConfirm={ this.onConfirmPopup }
        data={ this.state.deleteConfirmation.data }
        text='Do you want to delete the choreography?' /> : null;
    return (
      <PuppetOverviewDiv>
        { popup }
        <PuppetOverviewTitleDiv>
          <PuppetOverviewTitle>Puppets</PuppetOverviewTitle>
        </PuppetOverviewTitleDiv>
        <PuppetOverviewTilesDiv>
          { puppetTiles }
          <PuppetAddTile />
        </PuppetOverviewTilesDiv>
      </PuppetOverviewDiv>
    );
  }

  createPuppetTiles() {
    const puppets = this.state.puppets;
    return puppets.map((puppet, index) => {
      const onDeletePuppet = curry(this.onDeletePuppet)(puppet.identifier);
      return (
        <PuppetTile
          puppet={ puppet }
          key={ index }
          onDeletePuppet={ onDeletePuppet } />
      );
    });
  }

  onDeletePuppet(identifier, event) {
    this.setState({
      deleteConfirmation: {
        visible: true,
        data: identifier
      }
    });
  }

  onCancelPopup(identifier, event) {
    this.setState({
      deleteConfirmation: {
        visible: false,
        data: -1
      }
    });
  }

  onConfirmPopup(identifier, event) {
    this.setState({
      deleteConfirmation: {
        visible: false,
        data: -1
      }
    });

    const url = '/halloween/puppet/' + identifier;
    fetch(url, { method: 'DELETE' })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          toast('Deleted puppet with id: ' + identifier, {
            type: toast.TYPE.INFO
          });
          const puppets = this.state.puppets.filter(
            puppet => puppet.identifier.toString() !== identifier.toString());
          this.setState({ puppets: puppets });
        } else if (data.error) {
          toast(
            'Could not delete puppet. The puppet is still used by choreographies ' +
            data.used_by.toString());
        }
      });
  }
}
