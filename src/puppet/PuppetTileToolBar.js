import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from 'react-router-dom';


const PuppetToolBarDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 2.5em;
  flex-grow: 1;
  justify-content: flex-end;
  
  @media (max-width: 600px) {
    padding: 0.4em 2.25em;
  }
`;

const PuppetToolBarIcon = styled.i`
  font-size: 1.2em;
`;

const StyledButton = styled.button`
  height: 100%;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <button onClick={ onClick }>
      <PuppetToolBarIcon className={ iconClass } />
      { text }
    </button>
  );
};

const LinkButton = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  return (
    <StyledButton>
      <PuppetToolBarIcon className={ iconClass } />
      { text }
    </StyledButton>
  );
};

const Spacer = styled.div`
  flex-grow: 1;
`;

const SmallSpacer = styled.div`
  width: 0.5em;
`;

export class PuppetTileToolBar extends React.Component {
  constructor() {
    super();
    this.onTriggerPuppet = this.onTriggerPuppet.bind(this)
  }

  render() {
    const identifier = this.props.puppet.identifier;
    const onTriggerPuppet = curry(this.onTriggerPuppet)(identifier);
    const onDeletePuppet = this.props.onDeletePuppet;
    const editUrl = '/puppet_builder/' + identifier;
    return (
      <PuppetToolBarDiv>
        <Link to={ editUrl }>
          <LinkButton
            iconClass="fas fa-edit" />
        </Link>
        <SmallSpacer />
        <Button
          iconClass="fas fa-trash-alt"
          onClick={ onDeletePuppet } />
        <Spacer />
        <Spacer />
        <Button
          iconClass="fas fa-play-circle"
          onClick={ onTriggerPuppet } />
      </PuppetToolBarDiv>
    );
  }

  onTriggerPuppet(identifier, event) {
    console.log("Triggering puppet with id: " + identifier);
    const url = "/halloween/puppet/" + identifier + "/trigger";
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          console.log("Triggered puppet with id: " + identifier);
          toast("Triggered puppet with id " + identifier, { type: toast.TYPE.INFO });
        } else {
          toast("Error while triggering puppet");
        }
      })
  }
}
