import React from 'react';

import styled from "styled-components";


const FileSelectorDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin: 1em;
`;

const FileSelectorInput = styled.input`
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1em;
`;

const PuppetBuilderImageSelectorTitle = styled.h2`
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.6em;
  flex-grow: 0;
  text-align: left;
  justify-self: left;
  display: inline;
`;

const PuppetImage = styled.img`
  border: solid thin;
  margin: 1em;
  padding: 0.5em;
  height: auto;
  object-fit: cover;
`;

export class PuppetBuilderFileSelector extends React.Component {
  render() {
    const onPuppetImageFileNameChange = this.props.onPuppetImageFileNameChange;
    const image = this.createImage();
    return (
      <FileSelectorDiv>
        <PuppetBuilderImageSelectorTitle>Image</PuppetBuilderImageSelectorTitle>
        <FileSelectorInput
          type='file'
          accept='image/*'
          onChange={ onPuppetImageFileNameChange }
          capture />
        { image }
      </FileSelectorDiv>
    );
  }

  createImage() {
    const imageData = this.props.imageData;
    if (imageData.type === 'none') {
      return null;
    } else if (imageData.type === 'url_data') {
      return <PuppetImage src={ imageData.data } alt='Uploaded' />
    } else if (imageData.type === 'url') {
      return <PuppetImage src={ imageData.data } alt='Puppet' />
    }
  }
}
