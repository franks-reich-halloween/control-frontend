import React from 'react';

import styled from "styled-components";
import { keyframes } from 'styled-components';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const PuppetBuilderTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
  text-align: center;
`;

const PuppetBuilderTitleText = styled.h2`
  margin: auto;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 0;
  
  @media (max-width: 600px) {
    font-size: 1.2em;
  }
`;

const PuppetNameInput = styled.input`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
  border: 0;
  padding: 0 0 0 0.5em;
  width: 10em;
  
  @media (max-width: 600px) {
    font-size: 1.2em;
  }
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const ToolbarButton = styled.button`
  margin: auto auto auto 0.5em;
`;

const PuppetTitleIcon = styled.i`
  font-size: 1.8em;
`;

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }
  
  to {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled(PuppetTitleIcon)`
  color: lightgray;
  animation: ${rotation} 2s linear infinite;
  text-align: center;
  margin: 0.5em;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <ToolbarButton onClick={ onClick }>
      <PuppetTitleIcon className={ iconClass } />
      { text }
    </ToolbarButton>
  );
};

export class PuppetBuilderTitle extends React.Component {
  constructor() {
    super();

    this.onTriggerPuppet = this.onTriggerPuppet.bind(this);
  }

  render () {
    const name = this.props.name;
    const onPuppetNameChange = this.props.onPuppetNameChange;
    const alreadySaved = this.props.alreadySaved;
    const onSavePuppet = this.props.onSavePuppet;
    const indicatorOrIcons = this.props.showBusyIndicator ?
      <LoadingIcon className="fas fa-sync"/> :
      <Button iconClass="far fa-save" onClick={onSavePuppet}/>;
    const triggerButton = alreadySaved ?
      <Button iconClass="fas fa-play" onClick={ this.onTriggerPuppet }/> : null;
    return (
      <PuppetBuilderTitleDiv>
        <PuppetBuilderTitleText>Puppet</PuppetBuilderTitleText>
        <PuppetNameInput
          value={name}
          onChange={onPuppetNameChange}/>
        <Spacer/>
        { indicatorOrIcons }
        { triggerButton }
      </PuppetBuilderTitleDiv>
    );
  }

  onTriggerPuppet(event) {
    const identifier = this.props.identifier;
    console.log("Triggering puppet with id: " + identifier);
    const url = "/halloween/puppet/" + identifier + "/trigger";
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          console.log("Triggered puppet with id: " + identifier);
          toast("Triggered puppet with id " + identifier, { type: toast.TYPE.INFO });
        } else {
          toast("Error while triggering puppet");
        }
      })
  }
}
