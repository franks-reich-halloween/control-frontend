import React from 'react';

import styled from "styled-components";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {PuppetBuilderTitle} from "./PuppetBuilderTitle";
import {PuppetOutputPinSelector} from "./PuppetOutputPinSelector";
import {PuppetBuilderFileSelector} from "./PuppetBuilderFileSelector";


const PuppetBuilderDiv = styled.div`
  border: solid thin;
  display: flex;
  flex-direction: column;
  margin: 0.5em;
`;

export class PuppetBuilder extends React.Component {
  constructor() {
    super();
    this.state = {
      puppet: {
        name: "New Puppet",
        output: 1
      },
      output_pins: [],
      saving: false,
      loading: [],
      imageData: {
        type: 'none',
        data: ''
      }
    };

    this.onPuppetNameChange = this.onPuppetNameChange.bind(this);
    this.onPuppetOutputChange = this.onPuppetOutputChange.bind(this);
    this.onPuppetImageFileNameChange = this.onPuppetImageFileNameChange.bind(this);
    this.onSavePuppet = this.onSavePuppet.bind(this);
  }

  componentDidMount() {
    let changeMode = false;
    if (this.props.match) {
      changeMode = !isNaN(this.props.match.params.identifier);
    }
    if (changeMode) {
      this.loadPuppet();
    } else {
      this.loadOutputPins();
    }
  }

  loadPuppet() {
    const identifier = parseInt(this.props.match.params.identifier, 10);
    const url = '/halloween/puppet/' + identifier;
    const loading = ['puppets'];
    this.setState({ loading: loading });
    fetch(url)
      .then(response => response.json())
      .then(data => {
        let loading = this.state.loading;
        loading = loading.filter(s => s !== 'puppets');
        const imageData = {
          type: 'url',
          data: '/halloween/puppet/image/' + data.identifier
        };
        this.setState({
          puppet: data,
          imageData: imageData,
          loading: loading
        });
        this.loadOutputPins();
      });
  }

  loadOutputPins() {
    const url = '/halloween/free_output_pin';
    const loading = ['output_pins'];
    this.setState({ loading: loading });
    fetch(url)
      .then(response => response.json())
      .then(data => {
        let loading = this.state.loading;
        loading = loading.filter(s => s !== 'output_pins');
        let puppet = this.state.puppet;
        if (!isNaN(puppet.identifier)) {
          data.push({ output: puppet.output, pin: -1 });
          this.setState({
            output_pins: data,
            loading: loading
          });
        } else {
          if (data.length > 0) {
            puppet.output = data[0].output;
          }
          this.setState({
            puppet: puppet,
            output_pins: data,
            loading: loading
          });
        }
      });
  }

  onPuppetNameChange(event) {
    const name = event.target.value;
    let puppet = this.state.puppet;
    puppet.name = name;
    this.setState({ puppet: puppet });
  }

  onPuppetOutputChange(event) {
    const output = event.target.value;
    let puppet = this.state.puppet;
    puppet.output = output;
    this.setState({ puppet: puppet });
  }

  onPuppetImageFileNameChange(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = (function() {
      const imageData = {
        type: 'url_data',
        data: reader.result,
        file: file
      };
      this.setState({ imageData: imageData });
    }).bind(this);
    if (file) {
      reader.readAsDataURL(file);
    }
  }

  onSavePuppet() {
    this.setState({ saving: true });
    if (this.state.imageData.type === 'none') {
      toast("Please provide an image", { type: toast.TYPE.ERROR });
    }

    if (!isNaN(this.state.puppet.identifier)) {
      const url = '/halloween/puppet/' + this.state.puppet.identifier;
      fetch(url, {
        method: 'POST',
        body: JSON.stringify(this.state.puppet) })
        .then(response => response.json())
        .then(data => {
          if (data.error) {
            toast(data.error, { type: toast.TYPE.ERROR });
          } else {
            toast("Puppet updated", { type: toast.TYPE.INFO });
            this.uploadImage();
          }
        });
    } else {
      const url = '/halloween/puppet';
      fetch(url, {
        method: 'POST',
        body: JSON.stringify(this.state.puppet) })
        .then(response => response.json())
        .then(data => {
          if (data.error) {
            toast(data.error, { type: toast.TYPE.ERROR });
          } else {
            this.setState({ puppet: data });
            toast(
              "Puppet created with identifier " + data.identifier, { type: toast.TYPE.INFO });
            this.uploadImage();
          }
        });
    }
  }

  uploadImage() {
    const imageData = this.state.imageData;
    if (imageData.type === 'url_data') {
      const file = imageData.file;
      const reader = new FileReader();
      reader.onloadend = (function() {
        const url = '/halloween/puppet/image/' + this.state.puppet.identifier;
        fetch(url, {
          method: 'POST',
          headers: { 'Content-Type': file.type },
          body: reader.result })
          .then(response => response.json())
          .then(data => {
            if (data.error) {
              toast(data.error, { type: toast.TYPE.ERROR });
            } else {
              toast('Uploaded image', { type: toast.TYPE.INFO });
              this.setState({
                imageData: {
                  type: 'url',
                  data: url
                }
              });
            }
            this.setState({ saving: false });
          });
      }).bind(this);
      reader.readAsArrayBuffer(file);
    } else {
      this.setState({ saving: false });
    }
  }

  render() {
    const name = this.state.puppet.name;
    const alreadySaved = !isNaN(this.state.puppet.identifier);
    const busy = this.state.saving || this.state.loading.length > 0;
    const outputPins = this.state.output_pins;
    const output = this.state.puppet.output;
    const imageData = this.state.imageData;
    const identifier = this.state.puppet.identifier;
    return (
      <PuppetBuilderDiv>
        <PuppetBuilderTitle
          name={ name }
          showBusyIndicator={ busy }
          alreadySaved={ alreadySaved }
          onSavePuppet={ this.onSavePuppet }
          identifier={ identifier }
          onPuppetNameChange={ this.onPuppetNameChange } />
        <PuppetOutputPinSelector
          outputPins={ outputPins }
          output={ output }
          onPuppetOutputChange={ this.onPuppetOutputChange } />
        <PuppetBuilderFileSelector
          imageData={ imageData }
          onPuppetImageFileNameChange={ this.onPuppetImageFileNameChange } />
      </PuppetBuilderDiv>
    );
  }
}
