import React from 'react';

import styled from "styled-components";


const PuppetOutputPinSelectorDiv = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0.5em;
`;

const PuppetOutputPinSelectorText = styled.p`
  border: none;
  margin: 0;
  padding-left: 0.5em;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1em;
  display: inline;
`;

const PuppetOutputPinSelectorSelect = styled.select`
  border: none;
  margin: 0 0 0 0.5em;
  padding-left: 0.5em;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1em;
  display: inline;
  width: 4em;
`;

export class PuppetOutputPinSelector extends React.Component {
  render() {
    const outputPinSelect = this.createOutputPinSelect();
    return (
      <PuppetOutputPinSelectorDiv>
        <PuppetOutputPinSelectorText>Output</PuppetOutputPinSelectorText>
        { outputPinSelect }
      </PuppetOutputPinSelectorDiv>
    );
  }

  createOutputPinSelect() {
    const onPuppetOutputChange = this.props.onPuppetOutputChange;
    const outputPins = this.props.outputPins;
    const output = this.props.output;
    if (outputPins.length > 0) {
      const options = this.createOutputPinOptions(outputPins, output);
      return (
        <PuppetOutputPinSelectorSelect
          onChange={ onPuppetOutputChange }
          value={ output } >
          { options }
        </PuppetOutputPinSelectorSelect>
      );
    } else {
      return null;
    }
  }

  createOutputPinOptions(outputPins, output) {
    return outputPins.map((outputPin, index) => (
      <option key={ index }>{ outputPin.output }</option>
    ));
  }
}
