import React from 'react';

import styled from "styled-components";
import { keyframes } from 'styled-components';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const StepMotorControllerBuilderTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
  text-align: center;
`;

const StepMotorControllerBuilderTitleText = styled.h2`
  margin: auto;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 0;
  
  @media (max-width: 600px) {
    font-size: 1.2em;
  }
`;

const StepMotorControllerNameInput = styled.input`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
  border: 0;
  padding: 0 0 0 0.5em;
  width: 10em;
  
  @media (max-width: 600px) {
    font-size: 1.2em;
  }
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const ToolbarButton = styled.button`
  margin: auto auto auto 0.5em;
`;

const StepMotorControllerTitleIcon = styled.i`
  font-size: 1.8em;
`;

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }
  
  to {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled(StepMotorControllerTitleIcon)`
  color: lightgray;
  animation: ${rotation} 2s linear infinite;
  text-align: center;
  margin: 0.5em;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <ToolbarButton onClick={ onClick }>
      <StepMotorControllerTitleIcon className={ iconClass } />
      { text }
    </ToolbarButton>
  );
};

export class StepMotorControllerBuilderTitle extends React.Component {
  constructor() {
    super();
  }

  render() {
    const name = this.props.name;
    const onSaveStepMotorController = this.props.onSaveStepMotorController;
    const onStepMotorControllerNameChange = this.props.onStepMotorControllerNameChange;
    const onTriggerStepMotorController = this.props.onTriggerStepMotorController;
    const alreadySaved = this.props.alreadySaved;
    const showBusyIndicator = this.props.showBusyIndicator;
    const indicatorOrIcons = showBusyIndicator ?
      <LoadingIcon className="fas fa-sync" /> :
      <Button iconClass="far fa-save" onClick={ onSaveStepMotorController } />;
    const triggerButton = alreadySaved ?
      <Button iconClass="fas fa-play" onClick={ onTriggerStepMotorController } /> : null;
    return (
      <StepMotorControllerBuilderTitleDiv>
        <StepMotorControllerBuilderTitleText>
          Step Motor Controller
        </StepMotorControllerBuilderTitleText>
        <StepMotorControllerNameInput
          onChange={ onStepMotorControllerNameChange }
          value={ name } />
        <Spacer />
        { indicatorOrIcons }
        { triggerButton }
      </StepMotorControllerBuilderTitleDiv>
    );
  }
}
