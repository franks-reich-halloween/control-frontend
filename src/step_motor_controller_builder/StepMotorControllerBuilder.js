import React from 'react';

import styled from "styled-components";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { StepMotorControllerBuilderTitle } from "./StepMotorControllerBuilderTitle";
import { StepMotorControllerParameters } from "./StepMotorControllerParameters";


const StepMotorControllerBuilderDiv = styled.div`
  border: solid thin;
  display: flex;
  flex-direction: column;
  margin: 0.5em;
`;

export class StepMotorControllerBuilder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stepMotorController: {
        name: 'Bats',
        parameters: {
          frequency: 0,
		      duty_cycle_start: 0,
		      duty_cycle_stop: 0,
		      increment: 0,
		      pause: 0
        }
      },
      saving: false,
      loading: []
    };

    this.onStepMotorControllerNameChange = this.onStepMotorControllerNameChange.bind(this);
    this.onSaveStepMotorController = this.onSaveStepMotorController.bind(this);
    this.onTriggerStepMotorController = this.onTriggerStepMotorController.bind(this);
    this.onFrequencyParameterChange = this.onFrequencyParameterChange.bind(this);
    this.onDutyCycleStartParameterChange = this.onDutyCycleStartParameterChange.bind(this);
    this.onDutyCycleStopParameterChange = this.onDutyCycleStopParameterChange.bind(this);
    this.onIncrementParameterChange = this.onIncrementParameterChange.bind(this);
    this.onPauseParameterChange = this.onPauseParameterChange.bind(this);
  }

  componentDidMount() {
    let changeMode = false;
    if (this.props.match) {
      changeMode = !isNaN(this.props.match.params.identifier);
    }
    if (changeMode) {
      this.loadStepMotorController();
    }
  }

  loadStepMotorController() {
    const url = '/halloween/step_motor_controller/' + this.props.match.params.identifier;
    const loading = ['controller'];
    this.setState({ loading: loading });
    fetch(url)
      .then(response => response.json())
      .then(data => {
        let loading = this.state.loading;
        loading = loading.filter(s => s !== 'controller');
        this.setState({
          stepMotorController: data,
          loading: loading
        });
      });
  }

  onStepMotorControllerNameChange(event) {
    const name = event.target.value;
    let stepMotorController = this.state.stepMotorController;
    stepMotorController.name = name;
    this.setState({ stepMotorController: stepMotorController });
  }

  onSaveStepMotorController() {
    this.setState({ saving: true });
    const update = !isNaN(this.props.match.params.identifier);
    if (update) {
      const url = '/halloween/step_motor_controller/' + this.props.match.params.identifier;
      fetch(url, {
        method: 'POST',
        body: JSON.stringify(this.state.stepMotorController) })
        .then(response => response.json())
        .then(data => {
          if (data.error) {
            toast(data.error, { type: toast.TYPE.ERROR });
          } else {
            toast('Step Motor Controller updated', { type: toast.TYPE.INFO });
          }
          this.setState({ saving: false });
        });
    }
  }

  onFrequencyParameterChange(event) {
    let stepMotorController = this.state.stepMotorController;
    stepMotorController.parameters.frequency = event.target.value;
    this.setState({ stepMotorController: stepMotorController });
  }

  onDutyCycleStartParameterChange(event) {
    let stepMotorController = this.state.stepMotorController;
    stepMotorController.parameters.duty_cycle_start = event.target.value;
    this.setState({ stepMotorController: stepMotorController });
  }

  onDutyCycleStopParameterChange(event) {
    let stepMotorController = this.state.stepMotorController;
    stepMotorController.parameters.duty_cycle_stop = event.target.value;
    this.setState({ stepMotorController: stepMotorController });
  }

  onIncrementParameterChange(event) {
    let stepMotorController = this.state.stepMotorController;
    stepMotorController.parameters.increment = event.target.value;
    this.setState({ stepMotorController: stepMotorController });
  }

  onPauseParameterChange(event) {
    let stepMotorController = this.state.stepMotorController;
    stepMotorController.parameters.pause = event.target.value;
    this.setState({ stepMotorController: stepMotorController });
  }

  onTriggerStepMotorController() {
    const identifier = this.state.stepMotorController.identifier;
    const url = '/halloween/step_motor_controller/' + identifier + '/trigger';
    fetch(url, { method: 'POST' })
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          toast(data.error, { type: toast.TYPE.ERROR });
        } else {
          toast('Step Motor Controller triggered', { type: toast.TYPE.INFO });
        }
      });
  }

  render() {
    const name = this.state.stepMotorController.name;
    const alreadySaved = !isNaN(this.state.stepMotorController.identifier);
    const showBusyIndicator = this.state.saving || this.state.loading.length > 0;
    const stepMotorController = this.state.stepMotorController;
    return (
      <StepMotorControllerBuilderDiv>
        <StepMotorControllerBuilderTitle
          alreadySaved={ alreadySaved }
          showBusyIndicator={ showBusyIndicator }
          onTriggerStepMotorController={ this.onTriggerStepMotorController }
          onSaveStepMotorController={ this.onSaveStepMotorController }
          onStepMotorControllerNameChange={ this.onStepMotorControllerNameChange }
          name={ name } />
        <StepMotorControllerParameters
          onFrequencyChange={ this.onFrequencyParameterChange }
          onDutyCycleStartChange={ this.onDutyCycleStartParameterChange }
          onDutyCycleStopChange={ this.onDutyCycleStopParameterChange }
          onIncrementChange={ this.onIncrementParameterChange }
          onPauseChange={ this.onPauseParameterChange }
          stepMotorController={ stepMotorController } />
      </StepMotorControllerBuilderDiv>
    );
  }
}
