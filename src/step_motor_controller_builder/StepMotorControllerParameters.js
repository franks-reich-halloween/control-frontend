import React from 'react';

import styled from "styled-components";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const StepMotorControllerBuilderParametersTitle = styled.h2`
  margin: auto;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.6em;
  display: inline;
  flex-grow: 0;
  
  @media (max-width: 600px) {
    font-size: 1.2em;
  }
`;

const InputDiv = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0.5em;
`;

const StepMotorControllerBuilderParametersText = styled.p`
  margin: auto;
  padding-left: 0.5em;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1em;
  display: inline;
`;

const StepMotorControllerParametersTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0.5em;
`;

const StepMotorControllerParametersDiv = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0.5em;
`;

const StepMotorControllerLabelsDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0.5em;
  align-items: flex-end;
`;

const StepMotorControllerInputsDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0.5em;
  align-items: flex-start;
  flex-grow: 1;
`;

const StepMotorControllerParametersInput = styled.input`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1em;
  display: inline;
  flex-grow: 1;
  border: 0;
  padding: 0em;
`;

export const StepMotorControllerParameters = (props) => {
  const stepMotorController = props.stepMotorController;
  const frequency = stepMotorController.parameters.frequency;
  const dutyCycleStart = stepMotorController.parameters.duty_cycle_start;
  const dutyCycleStop = stepMotorController.parameters.duty_cycle_stop;
  const increment = stepMotorController.parameters.increment;
  const pause = stepMotorController.parameters.pause;
  const onFrequencyChange = props.onFrequencyChange;
  const onDutyCycleStartChange = props.onDutyCycleStartChange;
  const onDutyCycleStopChange = props.onDutyCycleStopChange;
  const onIncrementChange = props.onIncrementChange;
  const onPauseChange = props.onPauseChange;
  return (
    <React.Fragment>
      <StepMotorControllerParametersTitleDiv>
        <StepMotorControllerBuilderParametersTitle>
          Parameters
        </StepMotorControllerBuilderParametersTitle>
      </StepMotorControllerParametersTitleDiv>
      <StepMotorControllerParametersDiv>
        <StepMotorControllerLabelsDiv>
          <InputDiv>
            <StepMotorControllerBuilderParametersText>
              Frequency
            </StepMotorControllerBuilderParametersText>
          </InputDiv>
          <InputDiv>
            <StepMotorControllerBuilderParametersText>
              Duty Cycle Start
            </StepMotorControllerBuilderParametersText>
          </InputDiv>
          <InputDiv>
            <StepMotorControllerBuilderParametersText>
              Duty Cycle Stop
            </StepMotorControllerBuilderParametersText>
          </InputDiv>
          <InputDiv>
            <StepMotorControllerBuilderParametersText>
              Increment
            </StepMotorControllerBuilderParametersText>
          </InputDiv>
          <InputDiv>
            <StepMotorControllerBuilderParametersText>
              Pause
            </StepMotorControllerBuilderParametersText>
          </InputDiv>
        </StepMotorControllerLabelsDiv>
        <StepMotorControllerInputsDiv>
          <InputDiv>
            <StepMotorControllerParametersInput
              onChange={ onFrequencyChange }
              value={ frequency } />
          </InputDiv>
          <InputDiv>
            <StepMotorControllerParametersInput
              onChange={ onDutyCycleStartChange }
              value={ dutyCycleStart } />
          </InputDiv>
          <InputDiv>
            <StepMotorControllerParametersInput
              onChange={ onDutyCycleStopChange }
              value={ dutyCycleStop } />
          </InputDiv>
          <InputDiv>
            <StepMotorControllerParametersInput
              onChange={ onIncrementChange }
              value={ increment } />
          </InputDiv>
          <InputDiv>
            <StepMotorControllerParametersInput
              onChange={ onPauseChange }
              value={ pause } />
          </InputDiv>
        </StepMotorControllerInputsDiv>
      </StepMotorControllerParametersDiv>
    </React.Fragment>
  );
};
