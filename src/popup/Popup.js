import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';


const OuterDiv = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  background-color: rgba(0,0,0, 0.5);
`;

const InnerDiv = styled.div`
  position: absolute;
  left: 25%;
  right: 25%;
  top: 25%;
  bottom: 25%;
  margin: auto;
  background: white;
  display: flex;
  flex-direction: column;
`;

const ContentDiv = styled.div`
  flex-grow: 1;
`;

const PopupToolbarDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em;
  justify-content: flex-end;
  align-items: stretch;
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const ToolbarIcon = styled.i`
  font-size: 1.2em;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <button onClick={ onClick }>
      <ToolbarIcon className={ iconClass } />
      { text }
    </button>
  );
};

const Text = styled.p`
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.0em;
`;

export class Popup extends React.Component {
  constructor() {
    super();
    this.onInnerClick = this.onInnerClick.bind(this);
  }

  render() {
    const text = this.props.text;
    const data = this.props.data;
    const onCancel = curry(this.props.onCancel)(data);
    const onConfirm = curry(this.props.onConfirm)(data);
    return (
      <OuterDiv
        onClick={onCancel}>
        <InnerDiv
          onClick={ this.onInnerClick }>
          <ContentDiv>
            <Text>{ text }</Text>
          </ContentDiv>
          <PopupToolbarDiv>
            <Button
              iconClass='fas fa-times-circle'
              text='No'
              onClick={ onCancel } />
            <Spacer/>
            <Button
              iconClass='fas fa-check-circle'
              text='Yes'
              onClick={ onConfirm }/>
          </PopupToolbarDiv>
        </InnerDiv>
      </OuterDiv>
    );
  }

  onInnerClick(event) {
    event.stopPropagation();
  }
}
