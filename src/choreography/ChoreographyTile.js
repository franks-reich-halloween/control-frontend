import React from 'react';
import { Link } from 'react-router-dom';

import styled from "styled-components";

import { ChoreographyTileToolbar } from "./ChoreographyTileToobar";


const ChoreographyName = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.2em;
  display: inline;
  padding-top: 0.5em;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex-grow: 1;
  
  @media (max-width: 600px) {
    padding-top: 0.25em;
    font-size: 1.0em;
  }
`;

const ChoreographyChildrenDiv = styled.div`
  border: solid thin;
  width: 15em;
  height: 15em;
  align-self: center;
  overflow-y: scroll;
  
  @media (max-width: 600px) {
    width: 6.5em;
    height: 6.5em;
  }
`;

const AddIcon = styled.i`
  font-size: 15em;
  margin: auto;
  
  @media (max-width: 600px) {
    font-size: 6.5em;
  }
`;

const ChoreographyTileDiv = styled.div`
  border: solid thin;
  width: 20em;
  height: 20em;
  margin: 1em;
  display: flex;
  flex-direction: column;
  
  @media (max-width: 600px) {
    width: 11em;
    height: 11em;
    margin: 0.3em;
  }
`;

const ChildrenText = styled.p`
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.0em;
`;

const ChoreographyBottomSpaceDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 0.4em 0.4em 0.4em;
  flex-grow: 1;
  justify-content: flex-end;
  
  @media (max-width: 600px) {
    padding: 0.4em 0.4em 0.4em 0.4em;
  }
`;

const TileLink = styled(Link)`
  margin: 0;
  font-style: normal;
  color: black;
  text-decoration: none;
  
  &:hover {
    background-color: lightgray;
  }
`;

export class ChoreographyTile extends React.Component {
  render() {
    const choreography = this.props.choreography;
    const name = choreography.name;
    const childrenList = this.createChildrenList();
    const onDeleteChoreography = this.props.onDeleteChoreography;
    const onRepeatedSchedule = this.props.onRepeatedSchedule;
    const onStopRepeatedSchedule = this.props.onStopRepeatedSchedule;
    const canBeStarted = this.props.canBeStarted;
    const canBeStopped = this.props.canBeStopped;
    return (
      <ChoreographyTileDiv>
        <ChoreographyName>{name}</ChoreographyName>
        <ChoreographyChildrenDiv>
          { childrenList }
        </ChoreographyChildrenDiv>
        <ChoreographyTileToolbar
          choreography={ choreography }
          canBeStarted={ canBeStarted }
          canBeStopped={ canBeStopped }
          onRepeatedSchedule={ onRepeatedSchedule }
          onStopRepeatedSchedule={ onStopRepeatedSchedule }
          onDeleteChoreography={ onDeleteChoreography } />
      </ChoreographyTileDiv>
    )
  }

  createChildrenList() {
    const choreography = this.props.choreography;
    return choreography.events.filter(event => event.type === "Puppet")
      .map((puppet, index) => ( <ChildrenText key={ index }>{ puppet.name }</ChildrenText> ));
  }
}

const AddTileInnerDiv = styled.div`
  width: 20em;
  height: 20em;
  margin: 0em;
  display: flex;
  flex-direction: column;
  
  @media (max-width: 600px) {
    width: 11em;
    height: 11em;
  }
`;

export const ChoreographyAddTile = () => (
    <ChoreographyTileDiv>
      <TileLink to="/choreography_builder">
      <AddTileInnerDiv>
      <ChoreographyName>New Choreography</ChoreographyName>
        <AddIcon className="fas fa-plus-circle" />
      <ChoreographyBottomSpaceDiv />
      </AddTileInnerDiv>
      </TileLink>
    </ChoreographyTileDiv>
);
