import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from 'react-router-dom';


const ChoreographyToolbarDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 2.5em;
  flex-grow: 1;
  justify-content: flex-end;
  align-items: stretch;
  
  @media (max-width: 600px) {
    padding: 0.4em 2.25em;
  }
`;

const ChoreographyToolbarIcon = styled.i`
  font-size: 1.2em;
`;

const StyledButton = styled.button`
  height: 100%;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <button onClick={ onClick }>
      <ChoreographyToolbarIcon className={ iconClass } />
      { text }
    </button>
  );
};

const LinkButton = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  return (
    <StyledButton>
      <ChoreographyToolbarIcon className={ iconClass } />
      { text }
    </StyledButton>
  );
};

const Spacer = styled.div`
  flex-grow: 1;
`;

const SmallSpacer = styled.div`
  width: 0.5em;
`;

export class ChoreographyTileToolbar extends React.Component {
  constructor() {
    super();
    this.onTriggerChoreography = this.onTriggerChoreography.bind(this)
  }

  render() {
    const identifier = this.props.choreography.identifier;
    const onTriggerChoreography = curry(this.onTriggerChoreography)(identifier);
    const onDeleteChoreography = this.props.onDeleteChoreography;
    const url = '/choreography_builder/' + identifier;
    const onRepeatedSchedule = this.props.onRepeatedSchedule;
    const onStopRepeatedSchedule = this.props.onStopRepeatedSchedule;
    const startButton = this.props.canBeStarted ?
      <Button
        iconClass="fas fa-play"
        onClick={ onRepeatedSchedule } /> : null;
    const stopButton = this.props.canBeStopped ?
      <Button
        iconClass="fas fa-stop"
        onClick={ onStopRepeatedSchedule() } /> : null;
    return (
      <ChoreographyToolbarDiv>
        <Link to={ url }>
          <LinkButton
            iconClass="fas fa-edit" />
        </Link>
        <SmallSpacer />
        <Button
          iconClass="fas fa-trash-alt"
          onClick={ onDeleteChoreography } />
        <Spacer />
        { startButton }
        { stopButton }
        <SmallSpacer />
        <Button
          iconClass="fas fa-play-circle"
          onClick={ onTriggerChoreography } />
      </ChoreographyToolbarDiv>
    );
  }

  onTriggerChoreography(identifier, event) {
    console.log("Triggering choreography with id: " + identifier);
    const url = "halloween/choreography/" + identifier + "/trigger";
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          console.log("Triggered choreography with id: " + identifier);
          toast("Triggered choreography with id " + identifier, {
            type: toast.TYPE.INFO
          });
        } else {
          toast("Error while triggering choreography");
        }
      })
  }
}
