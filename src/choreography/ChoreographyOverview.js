import React from 'react';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from "styled-components";
import { curry } from 'ramda';

import { ChoreographyTile, ChoreographyAddTile } from "./ChoreographyTile";
import { Popup } from "../popup/Popup";


const ChoreographyOverviewDiv = styled.div`
  display: flex;
  flex-direction: column;
`;

const ChoreographyOverviewTilesDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

const ChoreographyOverviewTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
  text-align: center;
`;

const ChoreographyOverviewTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
`;

const OuterDiv = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  background-color: rgba(0,0,0, 0.5);
`;

export class ChoreographyOverview extends React.Component {
  constructor() {
    super();
    this.state = {
      choreographies: [],
      deleteConfirmation: {
        visible: false,
        data: -1
      },
      repeatedScheduling: false
    };

    this.onDeleteChoreography = this.onDeleteChoreography.bind(this);
    this.onCancelPopup = this.onCancelPopup.bind(this);
    this.onConfirmPopup = this.onConfirmPopup.bind(this);
    this.onRepeatedSchedule = this.onRepeatedSchedule.bind(this);
    this.onStopRepeatedSchedule = this.onStopRepeatedSchedule.bind(this);
    this.fetchChoreographies = this.fetchChoreographies.bind(this);
  }

  componentDidMount() {
    this.fetchChoreographies();
  }

  fetchChoreographies() {
    const url = '/halloween/choreography';
    fetch(url)
      .then(response => response.json())
      .then(
        data => {
          this.setState(
            {
              choreographies: data,
              repeatedScheduling: false
            })
        });
  }

  render() {
    const choreographyTiles = this.createChoreographyTiles();
    const popup = this.state.deleteConfirmation.visible
      ? <Popup
          onCancel={ this.onCancelPopup }
          onConfirm={ this.onConfirmPopup }
          data={ this.state.deleteConfirmation.data }
          text='Do you want to delete the choreography?' /> : null;
    const repeatedScheduleOverlay = this.state.repeatedScheduling ? <OuterDiv/> : null;
    return (
      <ChoreographyOverviewDiv>
        { repeatedScheduleOverlay }
        { popup }
        <ChoreographyOverviewTitleDiv>
          <ChoreographyOverviewTitle>Choreographies</ChoreographyOverviewTitle>
        </ChoreographyOverviewTitleDiv>
        <ChoreographyOverviewTilesDiv>
          { choreographyTiles }
          <ChoreographyAddTile />
        </ChoreographyOverviewTilesDiv>
      </ChoreographyOverviewDiv>
    );
  }

  createChoreographyTiles() {
    const choreographies = this.state.choreographies;
    const hasRunningChoreography = choreographies
      .filter(choreography => choreography.running).length > 0;
    return choreographies.map((choreography, index) => {
      const onRepeatedSchedule = curry(this.onRepeatedSchedule)(choreography.identifier);
      const onStopRepeatedSchedule =
        curry(this.onStopRepeatedSchedule)(choreography.identifier);
      const onDeleteChoreography = curry(this.onDeleteChoreography)(choreography.identifier);
      return (
        <ChoreographyTile
          choreography={ choreography }
          onDeleteChoreography={ onDeleteChoreography }
          onRepeatedSchedule={ onRepeatedSchedule }
          onStopRepeatedSchedule={ onStopRepeatedSchedule }
          canBeStarted={ !hasRunningChoreography }
          canBeStopped={ choreography.running }
          key={ index } />
      );
    });
  }

  onStopRepeatedSchedule(identifier, event) {
    this.setState({ repeatedScheduling: true });
    const url = '/halloween/choreography/' + identifier + '/stop_repeat';
    fetch(url, { method: 'POST'})
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          this.fetchChoreographies();
        } else {
          this.setState({ repeatedScheduling: false });
        }
      })
      .catch(this.setState({ repeatedScheduling: false }));
  }

  onRepeatedSchedule(identifier, event) {
    this.setState({ repeatedScheduling: true });
    const url = '/halloween/choreography/' + identifier + '/start_repeat';
    fetch(url, { method: 'POST'})
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          this.fetchChoreographies();
        } else {
          this.setState({ repeatedScheduling: false });
        }
      })
      .catch(this.setState({ repeatedScheduling: false }));
  }

  onDeleteChoreography(identifier, event) {
    this.setState({
      deleteConfirmation: {
        visible: true,
        data: identifier
      }
    });
  }

  onCancelPopup(data, event) {
    this.setState({
      deleteConfirmation: {
        visible: false,
        data: -1
      }
    });
  }

  onConfirmPopup(identifier, event) {
    this.setState({
      deleteConfirmation: {
        visible: false,
        data: -1
      }
    });

    const url = '/halloween/choreography/' + identifier;
    fetch(url, { method: 'DELETE' })
      .then(response => response.json())
      .then(data => {
          if (data.success) {
            toast('Deleted choreography with id: ' + identifier, {
              type: toast.TYPE.INFO
            });
            const choreographies = this.state.choreographies.filter(
              choreography => choreography.identifier.toString() !== identifier.toString());
            this.setState({ choreographies: choreographies });
          }
        }
      );
  }
}
