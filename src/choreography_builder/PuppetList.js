import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';

import { PuppetListItem, PuppetListItemPause } from "./PuppetListItem";


const PuppetListDiv = styled.div`
  border: solid thin;
  display: flex;
  flex-direction: column;
  margin: 0.5em;
  padding-bottom: 0.5em;
  flex-grow: 1;
`;

export class PuppetList extends React.Component {
  render() {
    const puppetList = this.createPuppetList();
    const onAddPause = this.props.onAddPause;
    return (
      <PuppetListDiv>
        <PuppetListItemPause
          onAddPause={ onAddPause } />
        { puppetList }
      </PuppetListDiv>
    );
  }

  createPuppetList() {
    return this.props.puppets.map((puppet, index) => {
      const onAddPuppet = curry(this.props.onAddPuppet)(puppet);
      return (
        <PuppetListItem
          puppet={ puppet }
          key={ index }
          onAddPuppet={ onAddPuppet } />
      );
    });
  }
}