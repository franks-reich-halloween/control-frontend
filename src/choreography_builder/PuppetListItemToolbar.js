import React from 'react';

import styled from "styled-components";


const PuppetListItemToolbarDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 0.4em 0.4em 0.4em;
  flex-grow: 1;
  justify-content: flex-end;
  
  @media (max-width: 600px) {
    padding: 0.4em 0.4em 0.4em 0.4em;
  }
`;

const PuppetListItemToolbarIcon = styled.i`
  font-size: 1.2em;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <button onClick={ onClick }>
      <PuppetListItemToolbarIcon className={ iconClass } />
      { text }
    </button>
  );
};

export const PuppetListItemToolbar = (props) => {
  const onAddPuppet = props.onAddPuppet;
  return (
    <PuppetListItemToolbarDiv>
      <Button
        iconClass="fas fa-plus-circle"
        onClick={ onAddPuppet } />
    </PuppetListItemToolbarDiv>
  );
};

export const PuppetListItemPauseToolbar = (props) => {
  const onAddPause = props.onAddPause;
  return (
    <PuppetListItemToolbarDiv>
      <Button
        iconClass="fas fa-plus-circle"
        onClick={ onAddPause } />
    </PuppetListItemToolbarDiv>
  );
};
