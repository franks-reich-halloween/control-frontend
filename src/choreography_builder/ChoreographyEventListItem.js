import React from 'react';

import styled from "styled-components";

import { ChoreographyPauseEventListItemToolbar, ChoreographyPuppetEventListItemToolbar } from "./ChoreographyEventListItemToolbar";


const ChoreographyEventDiv = styled.div`
  border: solid thin;
  display: flex;
  flex-direction: column;
  margin: 0.5em;
  flex-grow: 1;
  padding: 0.5em;
`;

const ChoreographyEventTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.4em;
  display: inline;
  flex-grow: 1;
`;

const ChoreographyEventInputDiv = styled.div`
  display: flex;
  flex-direction: row;
`;

const ChoreographyEventText = styled.p`
  border: none;
  margin: 0;
  padding-left: 0.5em;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1em;
  display: inline;
`;

const ChoreographyEventInput = styled.input`
  border: none;
  margin: 0;
  padding-left: 0.5em;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1em;
  display: inline;
  width: 4em;
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

export const ChoreographyPuppetEventListItem = (props) => {
  const name = props.event.name;
  const position = props.event.position;
  const onDeleteEvent = props.onDeleteEvent;
  const onMoveUp = props.onMoveUp;
  const onMoveDown = props.onMoveDown;
  return (
    <ChoreographyEventDiv>
      <ChoreographyEventTitle>{ name }</ChoreographyEventTitle>
      <ChoreographyEventInputDiv>
        <Spacer />
        <ChoreographyEventText>Position</ChoreographyEventText>
        <ChoreographyEventInput value={ position } readOnly />
      </ChoreographyEventInputDiv>
      <ChoreographyPuppetEventListItemToolbar
        onDeleteEvent={ onDeleteEvent }
        onMoveUp={ onMoveUp }
        onMoveDown={ onMoveDown } />
    </ChoreographyEventDiv>
  );
};

export const ChoreographyPauseEventListItem = (props) => {
  const pauseLength = props.event.length;
  const position = props.event.position;
  const onDeleteEvent = props.onDeleteEvent;
  const onMoveUp = props.onMoveUp;
  const onMoveDown = props.onMoveDown;
  const onPauseTimeChange = props.onPauseTimeChange;
  const onPauseTimeIncrement = props.onPauseTimeIncrement;
  const onPauseTimeDecrement = props.onPauseTimeDecrement;
  return (
    <ChoreographyEventDiv>
      <ChoreographyEventTitle>Pause</ChoreographyEventTitle>
      <ChoreographyEventInputDiv>
        <ChoreographyEventText>Pause in seconds</ChoreographyEventText>
        <ChoreographyEventInput
          value={ pauseLength }
          onChange={ onPauseTimeChange } />
        <Spacer />
        <ChoreographyEventText>Position</ChoreographyEventText>
        <ChoreographyEventInput value={ position } readOnly />
      </ChoreographyEventInputDiv>
      <ChoreographyPauseEventListItemToolbar
        onDeleteEvent={ onDeleteEvent }
        onMoveUp={ onMoveUp }
        onMoveDown={ onMoveDown }
        onPauseTimeIncrement={ onPauseTimeIncrement }
        onPauseTimeDecrement={ onPauseTimeDecrement } />
    </ChoreographyEventDiv>
  );
};
