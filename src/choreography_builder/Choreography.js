import React from 'react';

import styled from "styled-components";
import { keyframes } from 'styled-components';
import { ChoreographyEventList } from "./ChoreographyEventList";


const ChoreographyDiv = styled.div`
  border: solid thin;
  display: flex;
  flex-direction: column;
  margin: 0.5em 0.5em 0.5em 0;
  flex-grow: 3;
`;

const ChoreographyTitleDiv = styled.div`
  display: flex;
  flex-direction: row;
`;

const ChoreographyTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
`;

const ChoreographyInput = styled.input`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1.8em;
  display: inline;
  flex-grow: 1;
  border: 0;
  padding: 0 0 0 0.5em;
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const ChoreographyTitleIcon = styled.i`
  font-size: 1.8em;
`;

const ToolbarButton = styled.button`
  margin: 0.5em 0.5em 0 0;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <ToolbarButton onClick={ onClick }>
      <ChoreographyTitleIcon className={ iconClass } />
      { text }
    </ToolbarButton>
  );
};

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }
  
  to {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled(ChoreographyTitleIcon)`
  color: lightgray;
  animation: ${rotation} 2s linear infinite;
  margin: 0.5em 0.5em 0 0;
`;

export const Choreography = (props) => {
  const name = props.choreography.name;
  const choreography = props.choreography;
  const identifier = props.choreography.identifier;
  const onDeleteEvent = props.onDeleteEvent;
  const onMoveUp = props.onMoveUp;
  const onMoveDown = props.onMoveDown;
  const onChoreographyNameChange = props.onChoreographyNameChange;
  const onPauseTimeChange = props.onPauseTimeChange;
  const onPauseTimeIncrement = props.onPauseTimeIncrement;
  const onPauseTimeDecrement = props.onPauseTimeDecrement;
  const saving = props.saving;
  const onSaveChoreography = props.onSaveChoreography;
  const onTriggerChoreography = props.onTriggerChoreography;
  const loadingOrSave = !saving ? <Button iconClass="far fa-save" onClick={ onSaveChoreography } /> : <LoadingIcon className="fas fa-sync" />;
  const playButton = isNaN(identifier) ? null : <Button iconClass="fas fa-play" onClick={ onTriggerChoreography } />;
  return (
    <ChoreographyDiv>
      <ChoreographyTitleDiv>
        <ChoreographyTitle>Choreography</ChoreographyTitle>
        <ChoreographyInput
          value={ name }
          onChange={ onChoreographyNameChange } />
        <Spacer />
        { loadingOrSave }
        { playButton }
      </ChoreographyTitleDiv>
      <ChoreographyEventList
        events={ choreography.events }
        onDeleteEvent={ onDeleteEvent }
        onMoveUp={ onMoveUp }
        onMoveDown={ onMoveDown }
        onPauseTimeChange={ onPauseTimeChange }
        onPauseTimeIncrement={ onPauseTimeIncrement }
        onPauseTimeDecrement={ onPauseTimeDecrement } />
    </ChoreographyDiv>
  );
};
