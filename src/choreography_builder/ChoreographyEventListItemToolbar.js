import React from 'react';

import styled from "styled-components";


const ChoreographyEventListItemToolbarDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.4em 0.4em 0.4em 0.4em;
  flex-grow: 1;
  justify-content: flex-end;
  
  @media (max-width: 600px) {
    padding: 0.4em 0.4em 0.4em 0.4em;
  }
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const ChoreographyEventListItemToolbarIcon = styled.i`
  font-size: 1.2em;
`;

const ToolbarButton = styled.button`
  margin: 0.2em;
`;

const Button = (props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <ToolbarButton onClick={ onClick }>
      <ChoreographyEventListItemToolbarIcon className={ iconClass } />
      { text }
    </ToolbarButton>
  );
};

export const ChoreographyPuppetEventListItemToolbar = (props) => {
  const onDeleteEvent = props.onDeleteEvent;
  const onMoveUp = props.onMoveUp;
  const onMoveDown = props.onMoveDown;
  return (
    <ChoreographyEventListItemToolbarDiv>
      <Button
        iconClass="fas fa-minus-circle"
        onClick={ onDeleteEvent } />
      <Button
        iconClass="fas fa-arrow-alt-circle-down"
        onClick={ onMoveDown } />
      <Button
        iconClass="fas fa-arrow-alt-circle-up"
        onClick={ onMoveUp }/>
    </ChoreographyEventListItemToolbarDiv>
  );
};

export const ChoreographyPauseEventListItemToolbar = (props) => {
  const onDeleteEvent = props.onDeleteEvent;
  const onMoveUp = props.onMoveUp;
  const onMoveDown = props.onMoveDown;
  const onPauseTimeIncrement = props.onPauseTimeIncrement;
  const onPauseTimeDecrement = props.onPauseTimeDecrement;
  return (
    <ChoreographyEventListItemToolbarDiv>
      <Button
        iconClass="fas fa-hourglass"
        onClick={ onPauseTimeIncrement } />
      <Button
        iconClass="far fa-hourglass"
        onClick={ onPauseTimeDecrement } />
      <Spacer />
      <Button
        iconClass="fas fa-minus-circle"
        onClick={ onDeleteEvent } />
      <Button
        iconClass="fas fa-arrow-alt-circle-down"
        onClick={ onMoveDown }/>
      <Button
        iconClass="fas fa-arrow-alt-circle-up"
        onClick={ onMoveUp } />
    </ChoreographyEventListItemToolbarDiv>
  );
};
