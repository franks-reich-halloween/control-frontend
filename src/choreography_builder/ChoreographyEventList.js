import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';

import { ChoreographyPuppetEventListItem, ChoreographyPauseEventListItem } from "./ChoreographyEventListItem";


const ChoreographyEventListDiv = styled.div`
  border: solid thin;
  display: flex;
  flex-direction: column;
  margin: 0.5em 0.5em 0.5em 0.5em;
  flex-grow: 1;
`;

const ChoreographyEventListTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.6em;
  display: inline;
  flex-grow: 1;
`;

export class ChoreographyEventList extends React.Component {
  render() {
    const eventList = this.createEventList();
    return (
      <ChoreographyEventListDiv>
        <ChoreographyEventListTitle>Events</ChoreographyEventListTitle>
        { eventList }
      </ChoreographyEventListDiv>
    );
  }

  createEventList() {
    const events = this.props.events;
    const onDeleteEvent = this.props.onDeleteEvent;
    const onMoveUp = this.props.onMoveUp;
    const onMoveDown = this.props.onMoveDown;
    const onPauseTimeChange = this.props.onPauseTimeChange;
    const onPauseTimeIncrement = this.props.onPauseTimeIncrement;
    const onPauseTimeDecrement = this.props.onPauseTimeDecrement;
    return events.map((e, index) => {
      let event = e;
      e["position"] = index + 1;
      const curriedOnDeleteEvent = curry(onDeleteEvent)(index);
      const curriedOnMoveUp = curry(onMoveUp)(index);
      const curriedOnMoveDown = curry(onMoveDown)(index);
      if (event.type === "Puppet") {
        return (
          <ChoreographyPuppetEventListItem
            event={ event }
            key={ index }
            onDeleteEvent={ curriedOnDeleteEvent }
            onMoveUp={ curriedOnMoveUp }
            onMoveDown={ curriedOnMoveDown } />
        );
      } else if (event.type === "Pause") {
        const curriedOnPauseTimeChange = curry(onPauseTimeChange)(index);
        const curriedOnPauseTimeIncrement = curry(onPauseTimeIncrement)(index);
        const curriedOnPauseTimeDecrement = curry(onPauseTimeDecrement)(index);
        return (
          <ChoreographyPauseEventListItem
            event={ event }
            key={ index }
            onDeleteEvent={ curriedOnDeleteEvent }
            onMoveUp={ curriedOnMoveUp }
            onMoveDown={ curriedOnMoveDown}
            onPauseTimeChange={ curriedOnPauseTimeChange }
            onPauseTimeIncrement={ curriedOnPauseTimeIncrement }
            onPauseTimeDecrement={ curriedOnPauseTimeDecrement } />
        );
      } else {
        return null;
      }
    })
  }
}
