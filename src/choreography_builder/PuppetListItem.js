import React from 'react';

import styled from "styled-components";

import { PuppetListItemToolbar, PuppetListItemPauseToolbar } from "./PuppetListItemToolbar";


const PuppetListItemDiv = styled.div`
  border: solid thin;
  margin: 0.5em 0.5em 0em;
`;

const PuppetListItemTitle = styled.h3`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.0em;
  display: block;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: left;
  padding: 0.5em;
  
  @media (max-width: 600px) {
    padding-top: 0.25em;
    font-size: 0.8em;
  }
  
`;

export const PuppetListItem = (props) => {
  const name = props.puppet.name;
  const onAddPuppet = props.onAddPuppet;
  return (
    <PuppetListItemDiv>
      <PuppetListItemTitle>{ name }</PuppetListItemTitle>
      <PuppetListItemToolbar
        onAddPuppet={ onAddPuppet } />
    </PuppetListItemDiv>
  );
};

export const PuppetListItemPause = (props) => {
  const onAddPause = props.onAddPause;
  return (
    <PuppetListItemDiv>
      <PuppetListItemTitle>Pause</PuppetListItemTitle>
      <PuppetListItemPauseToolbar
        onAddPause={ onAddPause } />
    </PuppetListItemDiv>
  );
};
