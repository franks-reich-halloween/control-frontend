import React from 'react';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from "styled-components";

import { PuppetList } from "./PuppetList";
import { Choreography } from "./Choreography";


const ChoreographyBuilderDiv = styled.div`
  display: flex;
  flex-direction: row;
`;

export class ChoreographyBuilder extends React.Component {
  constructor() {
    super();
    this.state = {
      puppets: [],
      choreography: {
        name: "Choreography 1",
        events: []
      },
      saving: false
    };

    this.onAddPuppet = this.onAddPuppet.bind(this);
    this.onAddPause = this.onAddPause.bind(this);
    this.onDeleteEvent = this.onDeleteEvent.bind(this);
    this.onMoveUp = this.onMoveUp.bind(this);
    this.onMoveDown = this.onMoveDown.bind(this);
    this.onChoreographyNameChange = this.onChoreographyNameChange.bind(this);
    this.onPauseTimeChange = this.onPauseTimeChange.bind(this);
    this.onPauseTimeIncrement = this.onPauseTimeIncrement.bind(this);
    this.onPauseTimeDecrement = this.onPauseTimeDecrement.bind(this);
    this.onSaveChoreography = this.onSaveChoreography.bind(this);
    this.onTriggerChoreography = this.onTriggerChoreography.bind(this);
  }

  componentDidMount() {
    const url = '/halloween/puppet';
    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ puppets: data }));
    if (this.props.match && this.props.match.params.identifier) {
      this.setState({ saving: true });
      const choreography_url = '/halloween/choreography/' + this.props.match.params.identifier;
      fetch(choreography_url)
        .then(response => response.json())
        .then(data => {
          this.setState({
            choreography: data,
            saving: false
          })
        })
    }
  }

  onSaveChoreography(event) {
    this.setState({ saving: true });
    if (isNaN(this.state.choreography.identifier)) {
      const url = '/halloween/choreography';
      const data = JSON.stringify(this.state.choreography);
      fetch(url, {
        method: "POST",
        body: data })
        .then(response => response.json())
        .then(data => {
          toast(
            "Choreography saved with identifier " + data.identifier,
            { type: toast.TYPE.INFO });
          this.setState({
            choreography: data,
            saving: false
          })
        });
    } else {
      const url = '/halloween/choreography/' + this.state.choreography.identifier;
      const data = JSON.stringify(this.state.choreography);
      fetch(url, {
        method: "POST",
        body: data })
        .then(response => response.json())
        .then(data => {
          toast("Choreography updated", {type: toast.TYPE.INFO});
          this.setState({
            saving: false
          })
        });
    }
  }

  onTriggerChoreography(event) {
    const identifier = this.state.choreography.identifier;
    console.log('Triggering choreography with id: ' + identifier);
    const url = '/halloween/choreography/' + identifier + '/trigger';
    fetch(url, { method: 'POST' })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          console.log("Triggered choreography with id: " + identifier);
          toast('Triggered choreography with id: ' + identifier, {
            type: toast.TYPE.INFO
          });
        } else {
          toast('Error while triggering choreography');
        }
      });
  }

  onAddPuppet(puppet, event) {
    const puppetEvent = {
      type: "Puppet",
      name: puppet.name,
      identifier: puppet.identifier
    };
    let choreography = this.state.choreography;
    choreography.events.push(puppetEvent);
    this.setState({ choreography: choreography });
  }

  onAddPause(event) {
    const pauseEvent = {
      type: "Pause",
      length: 1
    };
    let choreography = this.state.choreography;
    choreography.events.push(pauseEvent);
    this.setState({ choreography: choreography });
  }

  onDeleteEvent(index, event) {
    let choreography = this.state.choreography;
    choreography.events.splice(index, 1);
    this.setState({ choreography: choreography });
  }

  onMoveUp(index, event) {
    if (index > 0) {
      let choreography = this.state.choreography;
      const temp = choreography.events[index - 1];
      choreography.events[index - 1] = choreography.events[index]
      choreography.events[index] = temp;
      this.setState({ choreography: choreography });
    }
  }

  onMoveDown(index, event) {
    let choreography = this.state.choreography;
    if (index < choreography.events.length - 1) {
      const temp = choreography.events[index];
      choreography.events[index] = choreography.events[index + 1];
      choreography.events[index + 1] = temp;
      this.setState({ choreography: choreography });
    }
  }

  onChoreographyNameChange(event) {
    const name = event.target.value;
    let choreography = this.state.choreography;
    choreography.name = name;
    this.setState({ choreography: choreography });
  }

  onPauseTimeChange(index, event) {
    const time = Number(event.target.value);
    if ((!isNaN(time) && time >= 0) || event.target.value === "") {
      let choreography = this.state.choreography;
      let pause = choreography.events[index];
      pause.length = event.target.value;
      choreography.events[index] = pause;
      this.setState({ choreography: choreography });
    }
  }

  onPauseTimeIncrement(index, event) {
    let choreography = this.state.choreography;
    let pause = choreography.events[index];
    const time = Number(pause.length);
    if (!isNaN(time)) {
      pause.length = time + 1;
      choreography.events[index] = pause;
      this.setState({ choreography: choreography });
    }
  }

  onPauseTimeDecrement(index, event) {
    let choreography = this.state.choreography;
    let pause = choreography.events[index];
    const time = Number(pause.length);
    if (!isNaN(time) && time >= 1) {
      pause.length = time - 1;
      choreography.events[index] = pause;
      this.setState({ choreography: choreography });
    }
  }

  render() {
    const puppets = this.state.puppets;
    const choreography = this.state.choreography;
    const saving = this.state.saving;
    return (
      <ChoreographyBuilderDiv>
        <PuppetList
          puppets={ puppets }
          onAddPause={ this.onAddPause }
          onAddPuppet={ this.onAddPuppet } />
        <Choreography
          choreography={ choreography }
          saving={ saving }
          onDeleteEvent={ this.onDeleteEvent }
          onMoveUp={ this.onMoveUp }
          onMoveDown={ this.onMoveDown }
          onChoreographyNameChange={ this.onChoreographyNameChange }
          onPauseTimeChange={ this.onPauseTimeChange }
          onPauseTimeIncrement={ this.onPauseTimeIncrement }
          onPauseTimeDecrement={ this.onPauseTimeDecrement }
          onSaveChoreography={ this.onSaveChoreography }
          onTriggerChoreography={ this.onTriggerChoreography }/>
      </ChoreographyBuilderDiv>
    );
  }
}
