import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import halloween_background from './halloween.jpg';
import './App.css';

import styled from 'styled-components';

import { MainMenu } from "./main_menu/MainMenu";
import { PuppetOverview } from "./puppet/PuppetOverview";
import { ToastContainer } from 'react-toastify';
import { ChoreographyBuilder } from "./choreography_builder/ChoreographyBuilder";
import { ChoreographyOverview } from "./choreography/ChoreographyOverview";
import { PuppetBuilder } from "./puppet_builder/PuppetBuilder";
import { StepMotorControllerOverview } from "./step_motor_controller/StepMotorControllerOverview";
import { StepMotorControllerBuilder } from "./step_motor_controller_builder/StepMotorControllerBuilder";


const Header = styled.header`
  background: black url(${halloween_background}) no-repeat center;
  background-size: cover;
`;

const AppTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 3em;
  padding-top: 0.5em;
  color: crimson;
  text-align: left;
  
  @media (max-width: 600px) {
    font-size: 2.0em;
  }
`;

const HeaderLink = styled(Link)`
  color: black;
  text-decoration: none;
  
  &:hover {
    background-color: lightgray;
  }
`;

class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <div className="App">
              <HeaderLink to='/'>
                <Header className="App-header">
                  <AppTitle className="App-title">Frank's Reich Halloween</AppTitle>
                </Header>
              </HeaderLink>
              <div>
                <Route
                  exact path="/"
                  component={ MainMenu } />
                <Route
                  path="/puppets"
                  component={ PuppetOverview } />
                <Route
                  exact path="/choreography_builder"
                  component={ ChoreographyBuilder }/>
                <Route
                  path="/choreography_builder/:identifier"
                  component={ ChoreographyBuilder }/>
                <Route
                  path="/choreographies"
                  component={ ChoreographyOverview }/>
                <Route
                  exact path="/puppet_builder"
                  component={ PuppetBuilder } />
                <Route
                  path="/puppet_builder/:identifier"
                  component={ PuppetBuilder } />
                <Route
                  path="/step_motor_controllers"
                  component={ StepMotorControllerOverview } />
                <Route
                  path='/step_motor_controller_builder/:identifier'
                  component={ StepMotorControllerBuilder } />
              </div>
            <ToastContainer autoClose={ 3000 } />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
